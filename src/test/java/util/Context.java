package util;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;

public class Context {

    private String path;
    private Response response;
    private List eventIds;

    public void setContentType(String type){
        given().contentType(type);
    }

    public void setBaseURI(String baseURI){
        RestAssured.baseURI = baseURI;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public List getEventIds() {
        return eventIds;
    }

    public void setEventIds(List eventIds) {
        this.eventIds = eventIds;
    }
}
