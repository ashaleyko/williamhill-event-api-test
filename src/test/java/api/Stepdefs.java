package api;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.json.JSONObject;
import util.Context;

import java.util.List;
import java.util.Random;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertTrue;

public class Stepdefs {

    private Context context;

    public Stepdefs(Context context) {
        this.context = context;
    }

    @Given("base URI is {string}")
    public void base_URI_is(String baseURI) {
        context.setBaseURI(baseURI);
    }

    @Given("content-type is application-json")
    public void content_type_is_application_json() {
        context.setContentType("application/json");
    }

    @Given("path is \\/events")
    public void path_is_events() {
        context.setPath("/events");
    }

    @When("I send GET request")
    public void i_send_GET_request() {
        context.setResponse(get(context.getPath()));
    }

    @Then("I receive four events in response")
    public void i_receive_four_events_in_response() {
        Response response = context.getResponse();
        List eventIds = response.jsonPath().getList("events.id");

        assertThat(eventIds.size(), equalTo(4));
    }

    @Then("Tennis and Football events are present")
    public void tennis_and_Football_events_are_present() {
        Response response = context.getResponse();
        List eventCategories = response.jsonPath().getList("events.category");

        assertTrue(eventCategories.contains("Tennis"));
        assertTrue(eventCategories.contains("Football"));
    }

    @Given("path is \\/event\\/eventId\\/market\\/marketId\\/addSelection")
    public void path_is_event_eventId_market_marketId_addSelection() {
        context.setPath("/event/1/market/122/addSelection");
    }

    @When("I send POST request")
    public void i_send_POST_request() {
        JSONObject payload = new JSONObject()
                .put("id", Integer.parseInt(String.format("%04d", new Random().nextInt(10000))))
                .put("selection", "Oleksandr Zinchenko")
                .put("status", "Active")
                .put("price", 5)
                .put("result", "");

        given().contentType("application/json").body(payload.toString()).post(context.getPath());
    }

    @Then("Selection is created")
    public void selection_is_created() {
        Response response = get("/events");
        List selections = response.jsonPath().getList("events[0].markets[1].selections.selection");

        assertTrue(selections.contains("Oleksandr Zinchenko"));
    }

    @Given("path is \\/event\\/eventId")
    public void path_is_event_eventId() {
        Response response = get("/events");
        List tennisEventsIds = response.jsonPath().getList("events.findAll { it.category == 'Tennis' }.id");
        context.setEventIds(tennisEventsIds);
    }

    @When("I send DELETE request for each Tennis event")
    public void i_send_DELETE_request_for_each_Tennis_event() {
        context.getEventIds().forEach(id -> delete("/event/" + id));
    }

    @Then("all Tennis events are deleted")
    public void all_Tennis_events_are_deleted() {
        Response response = get("/events");
        List tennisEventsIds = response.jsonPath().getList("events.findAll { it.category == 'Tennis' }.id");

        assertTrue(tennisEventsIds.isEmpty());
    }
}
