Feature: Event API testing
  The goal is to automate mock William Hill Event API testing

  Background:
    Given base URI is "http://localhost:49160"
    Given content-type is application-json

  Scenario: Retrieve all events
    Given path is /events
    When I send GET request
    Then I receive four events in response
    And Tennis and Football events are present

  Scenario: Create new selection
    Given path is /event/eventId/market/marketId/addSelection
    When I send POST request
    Then Selection is created

  Scenario: Delete all Tennis events
    Given path is /event/eventId
    When I send DELETE request for each Tennis event
    Then all Tennis events are deleted
