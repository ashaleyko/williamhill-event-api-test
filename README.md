# williamhill-event-api-test

This is simple API testing demonstration which is based on Java, Cucumber, REST Assured and PicoContainer

## Installation

Pull out the code using [Git](https://git-scm.com/downloads):

```shell
git clone https://gitlab.com/ashaleyko/williamhill-event-api-test.git
```

## Usage

Run using [Maven](https://maven.apache.org/download.cgi) in command line:

```shell
cd williamhill-event-api-test
mvn test
```

## Test report location

```shell
target/cucumber-reports/index.html
```